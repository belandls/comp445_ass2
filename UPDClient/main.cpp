/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Samuel
 *
 * Created on March 12, 2016, 2:44 PM
 */

#include <cstdlib>

#include "Client.h"
#include "ListFileCommand.h"
#include "FileChunker.h"
#include "PutFileCommand.h"
#include "GetFileCommand.h"
#include <vector>
#include <sstream>
#include <dirent.h>
#include <sys/stat.h>
using namespace std;

string getAddInfo(string q) {
    cout << q;
    string in;
    getline(cin, in);
    return in;
}

void listFiles() {
    cout << "Here are the local files : " << endl;
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(".")) != NULL) {
        /* print all the files and directories within directory */
        struct stat statbuf;
        while ((ent = readdir(dir)) != NULL) {
            if (stat(ent->d_name, &statbuf)) {
                continue;
            }
            if (S_ISDIR(statbuf.st_mode)) {
                continue;
            }
            cout << ent->d_name << endl;
        }
        closedir(dir);
    } else {
        /* could not open directory */
        perror("");
    }
}

/*
 * 
 */
int main(int argc, char** argv) {
    Client c;
    if (c.init()) {
        string input;
        cout << "Server address : ";
        getline(cin, input);

        if (c.connect(input)) {
            //            vector<Command*> commands;
            //            Command* temp = new GetFileCommand();
            //            temp->setWorkingSocket(c._data_socket);
            //            commands.push_back(temp);
            int res;
            do {
                cout << endl << "What do you want to do :" << endl;
                cout << "0. List Files (local)" << endl;
                cout << "1. List Files (server)" << endl;
                cout << "2. Put File" << endl;
                cout << "3. Get File" << endl;
                cout << "4. Quit" << endl;

                res = 0;
                input.clear();
                cout << "Choice : ";
                getline(cin, input);
                stringstream tempStream(input);
                if (tempStream >> res) {
                    if (res < 0 || res > 4) {
                        cout << "Invalid choice." << endl;
                        continue;
                    }
                } else {
                    cout << "The choice must be a number!" << endl;
                    continue;
                }
                Command* selectedCommand = NULL;
                string addInfo;
                switch (res) {
                    case 0:
                        listFiles();
                        continue;
                        break;
                    case 1:
                        selectedCommand = new ListFileCommand();
                        addInfo = "";
                        break;
                    case 2:
                        selectedCommand = new PutFileCommand();
                        addInfo = getAddInfo("What file do you want to upload? : ");
                        break;
                    case 3:
                        selectedCommand = new GetFileCommand();
                        addInfo = getAddInfo("What file do you want to download? : ");
                        break;
                    case 4:
                        continue;
                }
                cout << endl;
                selectedCommand->SetCommunicationEntity(c.GetClient());
                if (selectedCommand->sendCommand(addInfo)) {
                    if (selectedCommand->performInitialNego()) {
                        if (selectedCommand->performDataTransfer()) {
                            selectedCommand->showResult();
                        }else{
                            cout << "There was an error transferring the data" << endl;
                        }
                        selectedCommand->cleanup();
                    }else{
                        cout << "There was an error during the handshake" << endl;
                    }
                } else {
                    cout << "There was an error sending the command" << endl;
                }
                cout << endl;
                delete selectedCommand;
            } while (res != 4);
            system("pause");
        }
    }

    return 0;
}

