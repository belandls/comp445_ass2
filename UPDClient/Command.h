/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Command.h
 * Author: Samuel
 *
 * Created on February 9, 2016, 11:18 AM
 */

#ifndef COMMAND_H
#define COMMAND_H
#include<string>
#include "CommunicationEntity.h"

using namespace std;
class Command {
private:
         Communication::CommunicationEntity* _ce;
public:
	Command();
	virtual ~Command();
        
        void SetCommunicationEntity( Communication::CommunicationEntity*);
        Communication::CommunicationEntity* const GetCommunicationEntity() ;

	virtual bool sendCommand(string) = 0;
	virtual bool performInitialNego()=0;
	virtual bool performDataTransfer() = 0;
        virtual void showResult() = 0;
        virtual void cleanup() = 0;

};

#endif /* COMMAND_H */

