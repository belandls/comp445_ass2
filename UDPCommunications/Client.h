/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Client.h
 * Author: Samuel
 *
 * Created on March 13, 2016, 4:34 PM
 */


#ifndef CLIENT_H
#define CLIENT_H
#define _EXPORT_ __declspec(dllexport)

#include "CommunicationEntity.h"


namespace  Communication {

    class Client : public CommunicationEntity {
    public:
        _EXPORT_ Client();
        _EXPORT_ Client(const Client& orig);
        _EXPORT_ virtual ~Client();

        _EXPORT_ bool Connect(string);

    private:
        sockaddr_in _connected_server;


    };
}


#endif /* CLIENT_H */

