/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Client.cpp
 * Author: Samuel
 * 
 * Created on March 13, 2016, 4:34 PM
 */

#include "Client.h"
#include<iostream>
using namespace std;
using namespace Communication;

Client::Client() {

}

Client::Client(const Client& orig) {
}

Client::~Client() {
}

bool Client::Connect(string addr) {

    int result;
    addrinfo * addResult = NULL, hints;
    ZeroMemory(&hints, sizeof (hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_UDP;

    result = getaddrinfo(addr.c_str(), GetPort().c_str(), &hints, &addResult);
    if (result != 0) {
        cerr << "getaddrinfo failed : " << result << endl;
        WSACleanup();
        return false;
    }
    int initSequence = rand() % 255;
    Packet* p = new Packet(FLAG_INIT, 0, initSequence, true);
    p->SetDestination(addResult);
    if (!BlindSend(p)) {
        return false;
    }
    if (WaitForRecv(1000)) {
        const Packet *p2 = BlindRecv();
        if (p2 != NULL && p2->IsAck() && p2->IsInit() && p2->GetAckNumber() == initSequence) {
            cout << "Handshake Step 2" << endl;
            SetSendingSequenceNumber(p2->GetAckNumber() + 1);
            SetReceivingSequenceNumber(p2->GetSeqNumber() + 1);
            Packet* p3 = new Packet(FLAG_INIT | FLAG_ACK, p2->GetSeqNumber(), 0, true);
            p3->SetDestination(addResult);
            if (!BlindSend(p3)) {
                delete p3;
                return false;
            } else {
                SetConnectedDestination(p3->GetDestination());
                delete p3;
                return true;
            }

        } else {
            return false;
        }
    } else {
        Packet* pErr = new Packet(FLAG_RESET, 255, 255, true);
        BlindSend(pErr);
        delete pErr;
        return false;
    }

    freeaddrinfo(addResult);
    delete p;
    return false;
}


