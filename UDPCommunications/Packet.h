/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Packet.h
 * Author: Samuel
 *
 * Created on March 13, 2016, 5:32 PM
 */
#define _WIN32_WINNT 0x0A00
#include<winsock2.h>
#include <ws2tcpip.h>

#define FLAG_ACK 1
#define FLAG_RESET 2
#define FLAG_DATA 4
#define FLAG_INIT 8

#ifndef PACKET_H
#define PACKET_H

class Packet {
public:
    Packet(const char*, int, sockaddr_in);
    Packet(unsigned char, unsigned char, unsigned char);
    Packet(unsigned char, unsigned char, unsigned char,bool);
    Packet(const Packet& orig);
    virtual ~Packet();
    
    static const int HEADER_SIZE = 7;
    
    int GetSizeOfPacket();
    
    void SetData(const unsigned char*, int);
    const unsigned char* GetRawRepresentation();
    void FillFromRawRepresentation(const unsigned char*, int);
    void SetDestination(addrinfo*);
    void SetDestination(sockaddr_in);
    sockaddr_in GetDestination() const;
    sockaddr_in GetSource() const;
    unsigned char GetAckNumber()const;
    unsigned char GetSeqNumber()const;
    const unsigned char * GetData();
    
    bool IsAck() const;
    bool IsInit() const;
    bool IsData() const;
private:
    int _size_of_data;
    unsigned char _flags;
    unsigned char _ack_number;
    unsigned char _seq_number;
    unsigned char* _packet_byte_representation;
    sockaddr_in _source;
    sockaddr_in _destination;
};

#endif /* PACKET_H */

