/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Logger.cpp
 * Author: Sam
 * 
 * Created on March 16, 2016, 11:42 AM
 */

#include "Logger.h"




Logger::Logger(string logName) {
    
    _out_file.open(logName.c_str(), std::ofstream::out | std::ofstream::app);
    if (_out_file.is_open()){
        _loaded = true;
        _out_file << "Starting a new session..." << endl;
    }else{
        _loaded = false;
    }
}

Logger::Logger(const Logger& orig) {
}

Logger::~Logger() {
}

void Logger::Log(string message) {
    if (_loaded){
        _out_file << message << endl;
    }
}

