/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CommunicationEntity.h
 * Author: Samuel
 *
 * Created on March 13, 2016, 4:33 PM
 */


#include <string>
#include "Logger.h"
#include "Packet.h"
using namespace std;

#ifndef COMMUNICATIONENTITY_H
#define COMMUNICATIONENTITY_H

#define _EXPORT_ __declspec(dllexport)
namespace Communication {

    class CommunicationEntity {
    public:
        CommunicationEntity();
        CommunicationEntity(const CommunicationEntity& orig);
        virtual ~CommunicationEntity();


        _EXPORT_ bool Initialize(string, bool, short);

        _EXPORT_ bool BlindSend(Packet*);
        _EXPORT_ const Packet* BlindRecv(bool = true);
        _EXPORT_ bool Send(const unsigned char*, int);
        _EXPORT_ const unsigned char* Recv();
        _EXPORT_ int GetLastPacketDataSize();
        _EXPORT_ void InitializeLog(string);
        //    _EXPORT_ bool Recv(Packet*, int);

    private:
        SOCKET _communication_socket;
        unsigned char _sending_sequence_number;
        unsigned char _receiving_sequence_number;
        string _port;
        Packet* _last_packet_received;
        sockaddr_in _connected_destination;
        short _loss_rate;
        Logger* _log;

        bool SetSequenceNumber(unsigned char&, unsigned char);
        void DumpAndAckResendsFromLostAck();

    protected:
        SOCKET GetCommunicationSocket() const;
        string GetPort() const;
        bool SetReceivingSequenceNumber(unsigned char);
        bool SetSendingSequenceNumber(unsigned char);
        unsigned char GetReceivingSequenceNumber()const;
        unsigned char GetSendingSequenceNumber()const;
        unsigned char GetNextSendingSequenceNumber()const;
        void SetConnectedDestination(sockaddr_in);
        bool WaitForRecv(long msec);
    };
}


#endif /* COMMUNICATIONENTITY_H */

