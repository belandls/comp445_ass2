/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CommunicationEntity.cpp
 * Author: Samuel
 * 
 * Created on March 13, 2016, 4:33 PM
 */

#include "CommunicationEntity.h"
#include <iostream>
#include <time.h>

using namespace Communication;

CommunicationEntity::CommunicationEntity() {
    _communication_socket = INVALID_SOCKET;
    _last_packet_received = NULL;
    _sending_sequence_number = 255;
    _receiving_sequence_number = 255;
}

CommunicationEntity::CommunicationEntity(const CommunicationEntity& orig) {
}

CommunicationEntity::~CommunicationEntity() {
    closesocket(_communication_socket);
    WSACleanup();
    delete _last_packet_received;
}

SOCKET CommunicationEntity::GetCommunicationSocket() const {
    return _communication_socket;
}

string CommunicationEntity::GetPort() const {
    return _port;
}

void CommunicationEntity::InitializeLog(string filename) {
    _log = new Logger(filename);
}


bool CommunicationEntity::Initialize(string port, bool bindSocket, short lossRate) {
    srand(time(NULL));
    _loss_rate = lossRate;
    int res;
    WSAData wsaData;
    addrinfo * addResult = NULL, hints;

    cout << "Initialising WSA...";
    res = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (res != 0) {
        cerr << "WSAStartup failed : " << res << endl;
        return false;
    } else {
        cout << "OK" << endl;
    }

    ZeroMemory(&hints, sizeof (hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_UDP;
    hints.ai_flags = AI_PASSIVE;

    res = getaddrinfo(NULL, port.c_str(), &hints, &addResult);
    if (res != 0) {
        cerr << "getaddrinfo failed : " << res << endl;
        WSACleanup();
        return false;
    }

    cout << "Creating communication socket...";
    _communication_socket = socket(addResult->ai_family, addResult->ai_socktype, addResult->ai_protocol);
    if (_communication_socket == INVALID_SOCKET) {
        cerr << "Socket creation Failed : " << WSAGetLastError() << endl;
        freeaddrinfo(addResult);
        WSACleanup();
        return false;
    } else {
        cout << "OK." << endl;
    }

    if (bindSocket) {
        cout << "Binding socket...";
        res = bind(_communication_socket, addResult->ai_addr, (int) addResult->ai_addrlen);
        if (res == SOCKET_ERROR) {
            cerr << "Socket binding Failed : " << WSAGetLastError() << endl;
            freeaddrinfo(addResult);
            closesocket(_communication_socket);
            WSACleanup();
            return false;
        } else {
            cout << "Ok." << endl;
        }
    }
    freeaddrinfo(addResult);

    _port = port;

    return true;

}

void CommunicationEntity::DumpAndAckResendsFromLostAck() {

    bool keep = true;
    if (_last_packet_received != NULL) {
        unsigned char toAck = _last_packet_received->GetSeqNumber();
        while (WaitForRecv(0)) {
            BlindRecv(keep);
            if (_last_packet_received != NULL && keep) {
                if (_last_packet_received->GetSeqNumber() == toAck) {
                    Packet* ackPacket = new Packet(FLAG_ACK, _last_packet_received->GetSeqNumber(), 255, true);
                    ackPacket->SetDestination(_connected_destination);
                    BlindSend(ackPacket);
                    delete ackPacket;
                } else {
                    keep = false;
                }
            }
        }
    }
}

const Packet* CommunicationEntity::BlindRecv(bool keepData) {

    char buffer[2048];
    sockaddr_in SenderAddr;
    int SenderAddrSize = sizeof (SenderAddr);

    int res = recvfrom(_communication_socket, buffer, 2048, 0, (SOCKADDR *) & SenderAddr, &SenderAddrSize);
    if (_last_packet_received != NULL && keepData) {
        delete _last_packet_received;
    }
    if (res == 0 || res == SOCKET_ERROR) {
        _last_packet_received = NULL;
    } else {
        if (keepData) {
            _last_packet_received = new Packet(buffer, res, SenderAddr);
        }
    }

    return _last_packet_received;
}

bool CommunicationEntity::BlindSend(Packet* p) {
    sockaddr_in dest = p->GetDestination();
    int res;
    if (rand() % 256 < _loss_rate) {
        res = 1;
    } else {
        res = sendto(_communication_socket, reinterpret_cast<const char*> (p->GetRawRepresentation()), p->GetSizeOfPacket(), 0, (sockaddr*) & dest, sizeof (dest));
    }
    if (res == 0 || res == SOCKET_ERROR) {
        return false;
    } else {
        return true;
    }

}

const unsigned char* CommunicationEntity::Recv() {
    bool validPacketReceived = false;
    const Packet* tempPack = NULL;
    while (!validPacketReceived) {
        tempPack = BlindRecv();
        if (tempPack != NULL && tempPack->IsData()) {
            if (!SetReceivingSequenceNumber(tempPack->GetSeqNumber() + 1)) {
                cout << "Duplicate Seq. Num. Received" << endl;
            } else {
                validPacketReceived = true;
            }
            Packet* ackPacket = new Packet(FLAG_ACK, tempPack->GetSeqNumber(), 255, true);
            ackPacket->SetDestination(_connected_destination);
            bool sendRes = BlindSend(ackPacket);
            delete ackPacket;
            if (!sendRes) {
                return NULL;
            }
        } else {

            return NULL;
        }
    }
    _last_packet_received = const_cast<Packet*> (tempPack);
    return _last_packet_received->GetData();
}

int CommunicationEntity::GetLastPacketDataSize() {
    if (_last_packet_received == NULL) {
        return -1;
    }
    return _last_packet_received->GetSizeOfPacket() - Packet::HEADER_SIZE;
}

bool CommunicationEntity::WaitForRecv(long msec) {
    fd_set recvSet;
    FD_ZERO(&recvSet);
    FD_SET(_communication_socket, &recvSet);
    timeval to;
    to.tv_sec = msec / 1000;
    to.tv_usec = (msec % 1000) * 1000;
    int res = select(0, &recvSet, NULL, NULL, &to);
    if (res == -1) {
        cout << WSAGetLastError() << endl;
    }
    return res > 0;
}

bool CommunicationEntity::Send(const unsigned char* data, int dataSize) {
    *_log << "Starting Stop/Wait Sending...";
    bool validSend = false;
    Packet* p = new Packet(FLAG_DATA, 255, GetSendingSequenceNumber(), false);
    p->SetData(data, dataSize);
    p->SetDestination(_connected_destination);
    while (!validSend) {
//        _log->Log("DumpAndResendAck");
        DumpAndAckResendsFromLostAck();
        if (BlindSend(p)) {
            bool isRecv = WaitForRecv(500);
            if (isRecv) {
                const Packet* rp = BlindRecv();
                if (rp == NULL) {
                    delete p;
                    return false;
                }
                if (rp->IsAck()) {
                    if (SetSendingSequenceNumber(rp->GetAckNumber() + 1)) {
                        validSend = true;
                    }
                }
            }
        } else {
            delete p;
            return false;
        }
    }
    delete p;
    cout << "Send successfull" << endl;
    return true;
}

void CommunicationEntity::SetConnectedDestination(sockaddr_in dest) {
    _connected_destination = dest;
}

unsigned char CommunicationEntity::GetNextSendingSequenceNumber() const {
    if (_sending_sequence_number == 255) {
        return 255;
    } else {
        return (_sending_sequence_number + 1) % 2;
    }
}

unsigned char CommunicationEntity::GetReceivingSequenceNumber() const {
    return _receiving_sequence_number;
}

unsigned char CommunicationEntity::GetSendingSequenceNumber() const {
    return _sending_sequence_number;
}

bool CommunicationEntity::SetSendingSequenceNumber(unsigned char sendSeq) {
    return SetSequenceNumber(_sending_sequence_number, sendSeq);
}

bool CommunicationEntity::SetReceivingSequenceNumber(unsigned char recSeq) {
    return SetSequenceNumber(_receiving_sequence_number, recSeq);
}

bool CommunicationEntity::SetSequenceNumber(unsigned char& seqToSet, unsigned char val) {
    unsigned char realLastSequence = 255;
    if (val > 1) {
        realLastSequence = val % 2;
    } else {
        realLastSequence = val;
    }

    if (seqToSet != 255 && seqToSet != realLastSequence || seqToSet == 255) {
        seqToSet = realLastSequence;
        return true;
    } else {
        return false;
    }
}


