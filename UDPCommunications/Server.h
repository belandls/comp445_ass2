/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Server.h
 * Author: Samuel
 *
 * Created on March 13, 2016, 4:34 PM
 */

#ifndef SERVER_H
#define SERVER_H

#include "CommunicationEntity.h"

namespace Communication {

    class Server : public CommunicationEntity {
    public:
        _EXPORT_ Server();
        _EXPORT_ Server(const Server& orig);
        _EXPORT_ virtual ~Server();

        _EXPORT_ void AwaitConnection();

    private:
        bool _connected;
        sockaddr_in _connected_client;
    };
}


#endif /* SERVER_H */

