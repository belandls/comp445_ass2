/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Packet.cpp
 * Author: Samuel
 * 
 * Created on March 13, 2016, 5:32 PM
 */

#include <string.h>

#include "Packet.h"
#include "Client.h"

Packet::Packet(const char* rawData, int size, sockaddr_in src) : _source(src){
    FillFromRawRepresentation(reinterpret_cast<const unsigned char*>(rawData),size);
}

Packet::Packet(unsigned char flags, unsigned char ackNum, unsigned char seqNum) : Packet(flags,ackNum,seqNum,false) {
    
}

Packet::Packet(unsigned char flags, unsigned char ackNum, unsigned char seqNum, bool empty) : _flags(flags), _ack_number(ackNum), _seq_number(seqNum) {
    _size_of_data = 0;
    if (empty){
        SetData(NULL,0);
    }
}


Packet::Packet(const Packet& orig) {
}

Packet::~Packet() {
}

void Packet::FillFromRawRepresentation(const unsigned char* rawBytes, int size) {

    _size_of_data = (rawBytes[0] << 24);
    _size_of_data = (rawBytes[1] << 16) | _size_of_data;
    _size_of_data = (rawBytes[2] << 8) | _size_of_data;
    _size_of_data = rawBytes[3] | _size_of_data;
    _flags = rawBytes[4];
    _ack_number = rawBytes[5];
    _seq_number = rawBytes[6];
    if (size > Packet::HEADER_SIZE){
        SetData(rawBytes + Packet::HEADER_SIZE,size - Packet::HEADER_SIZE);
    }else
    {
        SetData(NULL,0);
    }
}

const unsigned char* Packet::GetRawRepresentation() {
    return _packet_byte_representation;
}

int Packet::GetSizeOfPacket() {
    return _size_of_data + Packet::HEADER_SIZE;
}

void Packet::SetData(const unsigned char* source, int size) {
    _size_of_data = size;
    _packet_byte_representation = new unsigned char[_size_of_data + Packet::HEADER_SIZE];

    _packet_byte_representation[0] = (_size_of_data >> 24);
    _packet_byte_representation[1] = (_size_of_data >> 16);
    _packet_byte_representation[2] = (_size_of_data >> 8);
    _packet_byte_representation[3] = _size_of_data;
    _packet_byte_representation[4] = _flags;
    _packet_byte_representation[5] = _ack_number;
    _packet_byte_representation[6] = _seq_number;

    memcpy(_packet_byte_representation + Packet::HEADER_SIZE, source, _size_of_data);
}

void Packet::SetDestination(addrinfo* addr) {
    _destination = *((sockaddr_in*)addr->ai_addr);
}

void Packet::SetDestination(sockaddr_in dest){
    _destination = dest;
}

sockaddr_in Packet::GetDestination() const {
    return _destination;
}

sockaddr_in Packet::GetSource() const {
    return _source;
}

unsigned char Packet::GetAckNumber() const {
    return _ack_number;
}

unsigned char Packet::GetSeqNumber() const {
    return _seq_number;
}

const unsigned char* Packet::GetData() {
    return _packet_byte_representation + Packet::HEADER_SIZE;
}


bool Packet::IsAck() const {
    return _flags & FLAG_ACK;
}

bool Packet::IsInit() const {
    return _flags & FLAG_INIT;
}
    

bool Packet::IsData() const {
    return _flags & FLAG_DATA;
}






