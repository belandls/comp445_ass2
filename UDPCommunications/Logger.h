/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Logger.h
 * Author: Sam
 *
 * Created on March 16, 2016, 11:42 AM
 */

#include<fstream>
#include<string>
using namespace std;
#ifndef LOGGER_H
#define LOGGER_H

class Logger {
public:
    Logger(string);
    Logger(const Logger& orig);
    virtual ~Logger();

    void Log(string);
    template<typename T>
    friend Logger& operator<<(Logger & log,  T const& obj);

private:
    bool _loaded;
    ofstream _out_file;
};

template<typename T>
Logger& operator<<(Logger& log, T const& obj) {
    log._out_file << obj << endl;
    return log;
}

#endif /* LOGGER_H */

