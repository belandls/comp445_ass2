/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Server.cpp
 * Author: Samuel
 * 
 * Created on March 13, 2016, 4:34 PM
 */

#include "Server.h"
#include <iostream>

using namespace Communication;

Server::Server() {
    _connected = false;
}

Server::Server(const Server& orig) {
}

Server::~Server() {
}

void Server::AwaitConnection() {
    while (!_connected) {
        const Packet* p = BlindRecv();
        if (p != NULL && p->IsInit() && !p->IsAck()) {
            cout << "Handshake step 1" << endl;
            SetReceivingSequenceNumber(p->GetSeqNumber() + 1);

            int initSequence = rand() % 255;
            Packet* p2 = new Packet(FLAG_ACK | FLAG_INIT, p->GetSeqNumber(), initSequence, true);
            p2->SetDestination(p->GetSource());
            if (BlindSend(p2)) {
                cout << "Sent Ack" << endl;
                if (!WaitForRecv(1000)) {
                    cout << "Waited 1 sec" << endl;
                    Packet* pErr = new Packet(FLAG_RESET, 255, 255, true);
                    BlindSend(pErr);
                    delete pErr;
                    cout << "Reset Sent" << endl;
                } else {
                    const Packet* p3 = BlindRecv();
                    if (p3 != NULL && p3->IsAck() && p3->IsInit() && p3->GetAckNumber() == initSequence) {
                        _connected = true;
                        SetSendingSequenceNumber(p->GetAckNumber() + 1);
                        SetConnectedDestination(p3->GetSource());
                        std::cout << "Connected" << std::endl;
                    } else {
                        Packet* pErr = new Packet(FLAG_RESET, 255, 255, true);
                        BlindSend(pErr);
                        delete pErr;
                        cout << "Reset Sent" << endl;
                        cout << "Wrong ack received or ack not received" << endl;
                    }
                }
            } else {
                cout << "Error sending ACK" << endl;
            }
            delete p2;
        }
    }
}

