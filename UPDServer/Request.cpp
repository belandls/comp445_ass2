/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Request.cpp
 * Author: Samuel
 * 
 * Created on February 9, 2016, 3:44 PM
 */

#include <string.h>
#include "Request.h"

const int Request::REQUEST_SIZE = 260;

Request::Request() {
    
}

Request::Request(RequestType rt, int nob): Request(rt, nob,"") {
    
}

Request::Request(RequestType rt, int nob, string ai) :_req_type(rt), _size_of_data(nob),_add_info(ai) {
    if (_add_info.size() <255){
        _add_info.resize(255);
    }
    _raw_data_representation = NULL;
    updateByteRepresentation();
}

Request::RequestType Request::getRequestType() const{
    return _req_type;
}

int Request::getSizeOfData() const{
    return _size_of_data;
}

string Request::getAddInfo() const {
    return _add_info;
}



Request::Request(const Request& orig) {
}

Request::~Request() {
}

void Request::updateByteRepresentation() {
    if (_raw_data_representation != NULL){
        delete[] _raw_data_representation;
    }
    _raw_data_representation = new unsigned char[260];
    _raw_data_representation[0] = (char)_req_type;
    _raw_data_representation[1] = (_size_of_data >> 24);
    _raw_data_representation[2] = (_size_of_data >> 16);
    _raw_data_representation[3] = (_size_of_data >> 8);
    _raw_data_representation[4] = _size_of_data;
    if (_add_info.size() > 255){
        memcpy(_raw_data_representation+5,_add_info.substr(0).c_str(),255);
    }else{
        memcpy(_raw_data_representation+5,_add_info.c_str(),_add_info.size());
    }
    
}

void Request::fillFromRawByteRepresentation(const unsigned char* rawBytes) {
    
    _req_type = static_cast<RequestType>(rawBytes[0]);
    _size_of_data = 0;
    _size_of_data = (rawBytes[1] << 24);
    _size_of_data = (rawBytes[2] << 16) | _size_of_data;
    _size_of_data = (rawBytes[3] << 8) | _size_of_data;
    _size_of_data = rawBytes[4] | _size_of_data;
    unsigned char buff[255];
    memcpy(buff,rawBytes + 5, 255);
    _add_info.clear();
    _add_info.append(reinterpret_cast<char * const>(buff));
    
}

const unsigned char* Request::getRawByteRepresentation() {
    return _raw_data_representation;
}


