/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Samuel
 *
 * Created on March 13, 2016, 9:32 PM
 */

#include <cstdlib>
#include <iostream>
#include "Server.h"
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    Server s;
    if (s.init()){
        s.mainListenLoop();
    }
    system("pause");
    return 0;
}

