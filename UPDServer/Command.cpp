
/* 
 * File:   Command.cpp
 * Author: Samuel
 * Description : Implementation of the Command Class
 * Created on February 9, 2016, 11:18 AM
 */

#include "Command.h"

Command::Command() {
}

Command::~Command() {
}

 Communication::CommunicationEntity * const Command::GetCommunicationEntity()  {
    return _ce;
}

void Command::SetCommunicationEntity(Communication::CommunicationEntity* ce) {
    _ce = ce;
}
