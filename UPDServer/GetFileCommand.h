/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GetFileCommand.h
 * Author: Samuel
 *
 * Created on February 14, 2016, 3:37 PM
 */

#ifndef GETFILECOMMAND_H
#define GETFILECOMMAND_H

#include "Command.h"
#include "FileChunker.h"

class GetFileCommand : public Command {
public:
    GetFileCommand();
    GetFileCommand(const GetFileCommand& orig);
    virtual ~GetFileCommand();
    
    bool executeCommand(const char*) override;
    bool performDataTransfer() override;
    bool performInitialNego() override;
    
private:
    FileChunker* _file_chunker;
    
};

#endif /* GETFILECOMMAND_H */

