/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FileChunker.h
 * Author: Samuel Beland-Leblanc - 27185642
 * Description : Class allowing the reading of a file by chunk instead of loading the whole file in memory
 * Created on February 10, 2016, 12:18 PM
 */

#ifndef FILECHUNKER_H
#define FILECHUNKER_H
#include <string>
#include<fstream>
using namespace std;
class FileChunker {
public:
    FileChunker();
    FileChunker(const FileChunker& orig);
    virtual ~FileChunker();
    
    bool loadFile(string filename);     //Opens the file to chunk
    const char* getNextChunk();         //Gets the next chunk from the openend file
    int getLastChunkSize();             //Getter for _last_chunk_size
    bool isEOF();                       //Getter for _is_eof
    int getFileSize();                  //Getter for _file_size
    const static int CHUNK_MAX_SIZE;
    
private:

    ifstream* _in_stream;   //The handle to the file
    int _file_size;         //The size of the opened file
    bool _is_eof;           //Indicates if the file has been read completely
    bool _is_loaded;        //Indicates if a file has been loaded
    char* _last_chunk_read; //The last chunk that was read in memory
    int _last_chunk_size;   //The size of the last chunk that was read in memory
};

#endif /* FILECHUNKER_H */

