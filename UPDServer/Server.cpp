/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Server.cpp
 * Author: Samuel
 * 
 * Created on February 9, 2016, 11:20 AM
 */

#include "Server.h"
#include "ListFileCommand.h"
#include "PutFileCommand.h"
#include "Request.h"
#include "GetFileCommand.h"
#include <iostream>
using namespace std;

const char * Server::DEFAULT_PORT = "55555";

Server::Server() {
    _s = new Communication::Server();
}

Server::~Server() {
}

Communication::Server* const Server::GetServer() const {
    return _s;
}


bool Server::init() {

//    WSAData wsaData;
//    int result;
//    addrinfo * addResult = NULL, hints;
//
//    cout << "Initialising WSA...";
//    result = WSAStartup(MAKEWORD(2, 2), &wsaData);
//    if (result != 0) {
//        cerr << "WSAStartup failed : " << result << endl;
//        return false;
//    } else {
//        cout << "OK" << endl;
//    }
//
//    ZeroMemory(&hints, sizeof (hints));
//    hints.ai_family = AF_INET;
//    hints.ai_socktype = SOCK_STREAM;
//    hints.ai_protocol = IPPROTO_TCP;
//    hints.ai_flags = AI_PASSIVE;
//
//    result = getaddrinfo(NULL, DEFAULT_PORT, &hints, &addResult);
//    if (result != 0) {
//        cerr << "getaddrinfo failed : " << result << endl;
//        WSACleanup();
//        return false;
//    }
//
//    cout << "Creating listen socket...";
//    _listen_socket = socket(addResult->ai_family, addResult->ai_socktype, addResult->ai_protocol);
//    if (_listen_socket == INVALID_SOCKET) {
//        cerr << "Socket creation Failed : " << WSAGetLastError() << endl;
//        freeaddrinfo(addResult);
//        WSACleanup();
//        return false;
//    } else {
//        cout << "OK." << endl;
//    }
//
//    cout << "Binding socket...";
//    result = bind(_listen_socket, addResult->ai_addr, (int) addResult->ai_addrlen);
//    if (result == SOCKET_ERROR) {
//        cerr << "Socket binding Failed : " << WSAGetLastError() << endl;
//        freeaddrinfo(addResult);
//        closesocket(_listen_socket);
//        WSACleanup();
//        return false;
//    } else {
//        cout << "Ok." << endl;
//    }
//
//    freeaddrinfo(addResult);
//
//    cout << "Start listening...";
//    result = listen(_listen_socket, SOMAXCONN);
//    if (result == SOCKET_ERROR) {
//        cerr << "Listening Failed : " << WSAGetLastError() << endl;
//        closesocket(_listen_socket);
//        WSACleanup();
//        return false;
//    } else {
//        cout << "Ok." << endl;
//    }

    return _s->Initialize("9999",true,25);
}

void Server::mainListenLoop() {

    while (1) {
        cout << "Awaiting connection...";
        bool res = true;
        _s->AwaitConnection();
        if (!res) {
            
        } else {
            cout << "Connected." << endl;
            bool connected = true;
            while (connected) {
                
                const unsigned char* buffer  = _s->Recv();
                if (buffer == NULL) {
                    cout << "Disconnected" << endl;
                    connected = false;
                } else {
                    cout << "Command Received...";
                    Command* currentCommand = NULL;
                    Request rr;
                    rr.fillFromRawByteRepresentation(buffer);
                    if (rr.getRequestType() == Request::LIST_FILES) {
                        cout << "LIST_FILES" << endl;
                        currentCommand = new ListFileCommand();
                    } else if (rr.getRequestType() == Request::PUT_FILE) {
                        cout << "PUT_FILES" << endl;
                        currentCommand = new PutFileCommand();
                    }else if (rr.getRequestType() == Request::GET_FILE){
                        cout << "GET_FILES" << endl;
                        currentCommand = new GetFileCommand();
                    }
                    currentCommand->SetCommunicationEntity(_s);
                    if (currentCommand->executeCommand(rr.getAddInfo().c_str())){
                        if(currentCommand->performInitialNego()){
                            if(currentCommand->performDataTransfer()){
                                cout << "Command + tranfer successful" << endl;
                            }else{
                                cout << "There was an error transferring the data" << endl;
                            }
                        }else{
                            cout << "There was an error during the initial handshake" << endl;
                        }
                    }else{
                        cout << "There was an error executing the command" << endl;
                    }
                    delete currentCommand;
                }
            }
            cout << "Connection closed" << endl;
        }

    }


}

