/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MessageBuilder.cpp
 * Author: Samuel
 * 
 * Created on February 9, 2016, 12:06 PM
 */

#include "MessageBuilder.h"
#include <math.h>
#include<cstring>

const int MessageBuilder::CHUNK_SIZE = 5/*4098*/;

MessageBuilder::MessageBuilder() {
    addNewChunk();
}

MessageBuilder::MessageBuilder(const MessageBuilder& orig) {
}

MessageBuilder::~MessageBuilder() {
}

void MessageBuilder::addNewChunk() {
    Chunk c;
    c.data = new char[CHUNK_SIZE];
    c.length = 0;
    _chunks.push_back(c);
    _current_working_chunk = &(_chunks.back());
}

void MessageBuilder::updateCurrentChunk(char* data, int size) {
    std::memcpy(_current_working_chunk->data + _current_working_chunk->length, data, size);
    _current_working_chunk->length += size;
}


void MessageBuilder::addChar(char* data, int size) {
    int remainingSpace = CHUNK_SIZE - _current_working_chunk->length;
    int numOfChunkReq;
    if (size <= remainingSpace) {
        numOfChunkReq = 0;
        updateCurrentChunk(data,size);
    } else {
        int overflow = size - remainingSpace;
        numOfChunkReq = ceil((double) overflow / CHUNK_SIZE);
        updateCurrentChunk(data,remainingSpace);
    }

    
    int remainingData = size - remainingSpace;
    for (int i = 0; i < numOfChunkReq; i++) {
        addNewChunk();
        if (remainingData < CHUNK_SIZE) {
            updateCurrentChunk(data + (size - remainingData),remainingData);
        } else {
            updateCurrentChunk(data + (size - remainingData), CHUNK_SIZE);
            remainingData -= CHUNK_SIZE;
        }
    }
}


